% Compute spatial cost
global VOLUME
global START_FRAME
global PERIODS
global FRAME_COUNT
global FRAME_HEIGHT
global FRAME_WIDTH

% ??
global LAMBDA_S;
LAMBDA_S = 100;
global LAMBDA_T;
LAMBDA_T = 400;
global LAMBDA_static;
LAMBDA_static = 100;
global MIN_PERIODS;
MIN_PERIODS = 32;
global PERIODS_MULTIPLES;
PERIODS_MULTIPLES = 4;

read_video();

for x = 1:FRAME_WIDTH
    for y = 1:FRAME_HEIGHT
        g = repmat(VOLUME(y, x, 1, :),[1,1,1,2]);
        
        min_s = 100000;
        min_p = 100000;
        min_norm = 100000.0;
        
        for s = 1:FRAME_COUNT
            for p = 1:(FRAME_COUNT/2 - 2)
                window = g(:,:,:,s:s+p);
                test_window = g(:,:,:,s+p:s+p+p);
                
                diff = (window - test_window).^2;
                norm_value = sum(diff, 4);
                
                if norm_value < min_norm
                    min_s = s;
                    min_p = p;
                    min_norm = norm_value;
                end
            end
        end
        
        START_FRAME(y, x) = min_s;
        PERIODS(y, x) = min_p;
    end
end
            