function L = GCOCut(Dc,Sc)
% GraphCut used by gco code
% return 1-based index
%
    % scale for Int32 
    Dc = Dc .* 1000;
    
    frame_size = size(Dc);
    
    NumSites = prod(frame_size(1:2));
    NumLabels = frame_size(3);

    gch = GCO_Create(NumSites, NumLabels);             % Create new object with NumSites = FRAME_SIZE, NumLabels = FRAME_COUNT*2-1
    GCO_SetDataCost(gch, (reshape(Dc, [NumSites, NumLabels]))');
    GCO_SetSmoothCost(gch, Sc);
    
    GCO_Expansion(gch);
    L = GCO_GetLabeling(gch);
    L = reshape(L, frame_size(1:2));
    
end

