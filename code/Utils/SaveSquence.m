function [] = SaveSquence(I, file_path, file_name)

maxCount = size(I, 3);

up_limit = max(max(max(I)));

mat_list = im2uint8(mat2gray(I ./ up_limit));

if exist(file_path, 'dir') ~= 2
    mkdir(file_path);
end

image_list = zeros(size(I, 1), size(I, 2), 3, size(I, 3));
for t = 1:maxCount
    image_list(:,:,:, t) = ind2rgb(mat_list(:,:,t), parula(256));
    imwrite(image_list(:,:,:, t), cat(2, file_path, '\', file_name, '_', num2str(t), '.jpg'));
end

% implay(image_list);

vw = VideoWriter(cat(2, file_path, '\', file_name, '.avi'));
open(vw);
writeVideo(vw,image_list);
close(vw);

end