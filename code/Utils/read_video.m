% Read video
% file_name = input video file name
% if file_name = null, use debug video
function [] = read_video(file_name)
% 讀檔時會取得的global變數
global VOLUME
global START_FRAME
global PERIODS
global FRAME_COUNT
global FRAME_HEIGHT
global FRAME_WIDTH
global FRAME_CHANNEL
global FRAME_SIZE
global VIDEO_SCALE
global VIDEO_TARGET_WIDTH
global TEMPORAL_SCALE

    % Check argument number
    if nargin < 1
        % 使用debug用的矩陣
        FRAME_COUNT = 150;
        FRAME_CHANNEL = 1;
                
        FRAME_SIZE = FRAME_HEIGHT*FRAME_WIDTH;
        
        VIDEO_SCALE = 1;
                        
        % create Synthetic data
        VOLUME = zeros(64, 64, 1, 150);
        frame = zeros(64, 64);
        frame(1:64, 16:32) = 255;
        frame = floor(imgaussfilt(frame, [1, 2]));
        
        FRAME_WIDTH = 64*VIDEO_SCALE;
        FRAME_HEIGHT = 64*VIDEO_SCALE;
        
        for t = 1:150
            VOLUME(:,:,:,t) = imresize(circshift(frame,[0 t]), VIDEO_SCALE);
        end
        
        VOLUME = VOLUME(:,:,:,1:TEMPORAL_SCALE:end);
        FRAME_COUNT = size(VOLUME, 4);
    else
        % Create a VideoReader object to read data from the sample file. Then, determine the width and height of the video.
        xyloObj = VideoReader(file_name);
        
%         VIDEO_SCALE = 1/1;
        
        FRAME_WIDTH = xyloObj.Width*VIDEO_SCALE;
        FRAME_HEIGHT = xyloObj.Height*VIDEO_SCALE;
        FRAME_CHANNEL = 1;

        VOLUME = zeros(FRAME_HEIGHT,FRAME_WIDTH,1,FRAME_COUNT);
        
        % Read one frame at a time until the end of the video is reached.
        k = 1;
        if FRAME_CHANNEL == 1
            while hasFrame(xyloObj) && k <= 150
                VOLUME(:,:,:,k) = imresize(rgb2gray(readFrame(xyloObj)),VIDEO_SCALE);
                k = k+1;
            end
        else
            while hasFrame(xyloObj) && k <= 150
                VOLUME(:,:,:,k) = readFrame(xyloObj);
                k = k+1;
            end
        end
        
        FRAME_COUNT = k - 1;
        
        VOLUME = VOLUME(:,:,:,1:TEMPORAL_SCALE:end);
        FRAME_COUNT = size(VOLUME, 4);
        
        FRAME_SIZE = FRAME_HEIGHT*FRAME_WIDTH;
        
        disp('VOLUME size');
        size(VOLUME)
end

