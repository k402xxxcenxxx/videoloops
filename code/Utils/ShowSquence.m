function Selected = ShowSquence(I)
figure;

count = 1;
Selected = [];

maxCount = size(I, 3);

up_limit = max(max(max(I)));

h = imagesc(I(:, :, count));
caxis([0 up_limit])

hb1 = uicontrol('Style', 'PushButton', 'String', 'Next', ...
	'Callback', @NextBtnCB);
hb2 = uicontrol('Style', 'PushButton', 'String', 'Back', ...
	'Callback', @BackBtnCB);
hb1.Position(2) = hb2.Position(2)+1.1*hb2.Position(4);

   % Create slider
    sld = uicontrol('Style', 'slider',...
        'Min',1,'Max',maxCount,'Value',1,...
        'Position', [400 20 120 20],...
        'Callback', @SliderCB); 
    % Add a text uicontrol to label the slider.
    txt = uicontrol('Style','text',...
        'Position',[400 0 120 20],...
        'String',num2str(count));
while isempty(Selected)
	pause(.1);
end
	function NextBtnCB(src, evnt)
		if count < maxCount
			count = count + 1;
			h.CData = I(:, :, count);
            
            set(txt,'String',num2str(count));
		end
    end
	function BackBtnCB(src, evnt)
		if count > 1
			count = count - 1;
			h.CData = I(:, :, count);
            
            set(txt,'String',num2str(count));
		end
    end
	function SliderCB(hObject, eventdata, handles)
        slider_value = abs(floor(get(hObject,'Value')));
        set(txt,'String',num2str(count));
        count = slider_value;
        h.CData = I(:, :, count);
    end
end