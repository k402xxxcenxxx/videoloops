function [] = GenerateLoop(file_name,file_extension)
global VOLUME
global START_FRAME
global PERIODS
global FRAME_COUNT
global FRAME_HEIGHT
global FRAME_WIDTH
global FRAME_CHANNEL
global FRAME_SIZE
global VIDEO_SCALE

xyloObj = VideoReader('input.mp4');
VIDEO_SCALE = 1/4;
FRAME_WIDTH = xyloObj.Width * VIDEO_SCALE;
FRAME_HEIGHT = xyloObj.Height * VIDEO_SCALE;
FRAME_CHANNEL = 1;

VOLUME = zeros(FRAME_HEIGHT,FRAME_WIDTH,3,FRAME_COUNT,'uint8');
k = 1;
while hasFrame(xyloObj)
    VOLUME(:,:,:,k) = imresize(readFrame(xyloObj),VIDEO_SCALE);
    k = k+1;
end
FRAME_COUNT = k-1;

% wrapN = @(x, n) (1 + mod(x-1, n));
% time_mapping = @(t,s,p)(wrapN(wrapN(s + t, p), FRAME_COUNT));
% 
% s = reshape(START_FRAME,[FRAME_HEIGHT,FRAME_WIDTH]);
% p = reshape(PERIODS,[FRAME_HEIGHT,FRAME_WIDTH]);
% 
% loop_frames = FRAME_COUNT * 10;
% loop = zeros(FRAME_HEIGHT,FRAME_WIDTH,3,loop_frames,'uint8');
% for t = 1:loop_frames
%     for x = 1:FRAME_WIDTH
%         for y = 1:FRAME_HEIGHT
%             loop(y,x,:,t) = VOLUME(y,x,:,time_mapping(t,s(y,x),p(y,x)));
%         end
%     end
% end

vw = VideoWriter('input_small');
open(vw);
writeVideo(vw,VOLUME);
close(vw);
end

