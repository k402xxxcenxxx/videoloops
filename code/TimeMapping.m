function result = TimeMapping(sx,px,t)

mask = mod(t,px) < mod(sx,px);
% sx = sx -1;
% t = t - 1;
% if (mod(t,px) >= mod(sx,px))
%     result = sx - mod(sx,px) + mod(t,px);
% else
%     result = sx - mod(sx,px) + mod(t,px) + px;
% end
result = sx - mod(sx,px) + mod(t,px);
result(mask) = result(mask)+px;
end

