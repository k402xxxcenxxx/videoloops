function [] = AdvanceMethod_gpu(inputfilename, Smoothness, SpatialRate, TemporalRate, StaticRate, isNormalize)

outputfilename = cat(2, inputfilename, '_sm', num2str(Smoothness), '_sp', num2str(SpatialRate), '_t', num2str(TemporalRate), '_st', num2str(StaticRate));
if isNormalize == 1
    outputfilename = cat(2, outputfilename, '_normalize');
end

color_set = cat(1,summer,flipud(spring));
color_set(1,:) = [0.5 0.5 0.5];

% Compute spatial cost
global VOLUME
global START_FRAME
global PERIODS
global FRAME_COUNT
global FRAME_HEIGHT
global FRAME_WIDTH

global LAMBDA_S;
LAMBDA_S = 100;
global LAMBDA_T;
LAMBDA_T = 400;
global LAMBDA_static;
LAMBDA_static = 100;
global MIN_PERIODS;
MIN_PERIODS = 32;
global PERIODS_MULTIPLES;
PERIODS_MULTIPLES = 4;
global VIDEO_SCALE

if isempty(inputfilename)
    read_video();
    outputfilename = cat(2,'bar_move', outputfilename);
else
    % input video , initialize map
    read_video(inputfilename);
end

% Color normalize
if isNormalize == 1
    VOLUME = VOLUME ./ 255;
end

VOLUME_gpu = gpuArray(VOLUME);

START_FRAME = ones(FRAME_HEIGHT,FRAME_WIDTH);
PERIODS = ones(FRAME_HEIGHT,FRAME_WIDTH);

sxs = 1:1:FRAME_COUNT;
sxs = sxs(2:end);
pxs = 8:1:(FRAME_COUNT-1);
pxs = pxs(pxs + sxs(1) < FRAME_COUNT);

disp('Start compute VC VC_2...');
VC = cumsum(VOLUME,4);
VC_2 = cumsum((VOLUME .^2), 4);

disp('Start compute_static_cost...');
N = reshape(VOLUME(:,:,1,:),[FRAME_HEIGHT,FRAME_WIDTH,FRAME_COUNT]);
N = imgaussfilt3(N,[0.9,0.9,1.2]);

static_energy = min(1,LAMBDA_static * mad(abs(N(:,:,1:end-1) - N(:,:,2:end)),1,3));
static_energy(static_energy <= 0.1) = 0;

gamma_t = reshape((1 ./ (1 + LAMBDA_T * mad(abs(VOLUME(:,:,1,1:end-1) - VOLUME(:,:,1,2:end)),1,4))),[FRAME_HEIGHT,FRAME_WIDTH]);

global OPTIMAL_START_FRAME;
OPTIMAL_START_FRAME = ones(FRAME_HEIGHT, FRAME_WIDTH, length(pxs));
OSF = OPTIMAL_START_FRAME;

spatiotemporals = ones(FRAME_HEIGHT, FRAME_WIDTH,length(pxs)) * 1e3;
tic;
parfor px_index = 1:(length(pxs))
    px = pxs(px_index);
    candidateStartFrame = sxs(sxs + px <= FRAME_COUNT);% long periods may not have later start frame (sx + px <= frame_count)
    
    if length(candidateStartFrame) <= 1
        sx = candidateStartFrame(1);
        
        spatial_cost = zeros(FRAME_HEIGHT,FRAME_WIDTH);
        temporal_cost = zeros(FRAME_HEIGHT,FRAME_WIDTH);
        
        % V(x,sx)
        v1_index = sx;
        % V(x,sx+px)
        v2_index = sx+px;
        % V(x,sx-1)
        v3_index = sx - 1; % must be positive
        % V(x,sx+px-1)
        v4_index = sx + px -1;

        % ||V(x,sx) - V(x,sx+px)||^2 + ||V(x,sx-1) - V(x,sx+px-1)||^2
        temporalCost = reshape((VOLUME(:,:,:,v1_index) - VOLUME(:,:,:,v2_index)).^2 + (VOLUME(:,:,:,v3_index) - VOLUME(:,:,:,v4_index)).^2, [FRAME_HEIGHT,FRAME_WIDTH]);
        temporalCost = temporalCost .* gamma_t;
                
        for x_x = 1:FRAME_WIDTH
            for x_y = 1:FRAME_HEIGHT
                Dir_x = [-1,1,0,0];
                Dir_y = [0,0,-1,1];
                % Loop for all z
                for i = 1:length(Dir_x)
                    % Check boundary
                    if x_x + Dir_x(i) > 0 && x_x + Dir_x(i) <= FRAME_WIDTH
                        if x_y + Dir_y(i) > 0 && x_y + Dir_y(i) <= FRAME_HEIGHT

                            z_x = x_x + Dir_x(i);
                            z_y = x_y + Dir_y(i);

                            pz = px;
                            sz = sx;
                            
                            gamma_s = 1 / (1 + LAMBDA_S * mad(abs(double(VOLUME(x_y,x_x,1,:)) - double(VOLUME(z_y,z_x,1,:))),1,4));

                            result = 0;
                            for t = 1:px
                                result = result + sum((VOLUME(x_y, x_x, 1, TimeMapping(sx,px,t)) - VOLUME(x_y, x_x, 1, TimeMapping(sz,pz,t))).^2 + (VOLUME(z_y, z_x, 1, TimeMapping(sx,px,t)) - VOLUME(z_y, z_x, 1, TimeMapping(sz,pz,t))).^2,4);
                            end

                            spatial_cost(x_y, x_x) = spatial_cost(x_y, x_x) + (result / px * gamma_s);
                            % Loop for label end
                        end
                    end
                end
            end
        end
        
        spatiotemporals(:,:,px_index) = SpatialRate * spatial_cost + TemporalRate * temporalCost;
        OSF(:,:,px_index) = sx;
        
    else

        % V(x,sx)
        v1_index = candidateStartFrame;
        % V(x,sx+px)
        v2_index = min(FRAME_COUNT, candidateStartFrame + px);
        % V(x,sx-1)
        v3_index = max(1, candidateStartFrame - 1); % must be positive
        % V(x,sx+px-1)
        v4_index = candidateStartFrame + px -1;

        % ||V(x,sx) - V(x,sx+px)||^2 + ||V(x,sx-1) - V(x,sx+px-1)||^2
        temporalCost = reshape((VOLUME(:,:,:,v1_index) - VOLUME(:,:,:,v2_index)).^2 + (VOLUME(:,:,:,v3_index) - VOLUME(:,:,:,v4_index)).^2, [FRAME_HEIGHT,FRAME_WIDTH,length(candidateStartFrame)]);

        temporalCost = temporalCost .* repmat(gamma_t,1,1,length(candidateStartFrame));

        % Use min temporalCost to init s
        [minValues,minIndics] = min(temporalCost,[],3);

        sub_optimal_start_frame = candidateStartFrame(minIndics);
        
    %% Compute spatial cost

        spatial_cost_gpu = zeros(FRAME_HEIGHT,FRAME_WIDTH,length(candidateStartFrame),'gpuArray');

        Dir_x = gpuArray([-1,1,0,0]);
        Dir_y = gpuArray([0,0,-1,1]);

        x_indices = gpuArray(cat(3,repmat((1:FRAME_HEIGHT)',1,FRAME_WIDTH),repmat(1:FRAME_WIDTH,FRAME_HEIGHT,1)));
        for i = 1:length(Dir_x)
            z_indices = gpuArray(cat(3,min(max(x_indices(:,:,1) + Dir_y(i),1),FRAME_HEIGHT),min(max(x_indices(:,:,2) + Dir_x(i),1),FRAME_WIDTH)));
            sz = gpuArray(sub_optimal_start_frame(sub2ind([FRAME_HEIGHT,FRAME_WIDTH],z_indices(:,:,1),z_indices(:,:,2))));

            z_mask = gpuArray(zeros(FRAME_HEIGHT,FRAME_WIDTH));
            z_mask(x_indices(:,:,1) + Dir_y(i) <= FRAME_HEIGHT & x_indices(:,:,1) + Dir_y(i) >= 1 & x_indices(:,:,2) + Dir_x(i) <= FRAME_WIDTH & x_indices(:,:,2) + Dir_x(i) >= 1) = 1;

            gamma_s = 1 ./ (1 + LAMBDA_S * mad(abs(VOLUME_gpu - VOLUME_gpu(sub2ind(size(VOLUME_gpu),repmat(z_indices(:,:,1),[1,1,1,FRAME_COUNT]),repmat(z_indices(:,:,2),[1,1,1,FRAME_COUNT]), ones(FRAME_HEIGHT,FRAME_WIDTH,1,FRAME_COUNT), repmat(reshape(1:FRAME_COUNT,[1,1,1,FRAME_COUNT]),[FRAME_HEIGHT,FRAME_WIDTH,1,1])))),1,4));


            for sx_index = 1:length(candidateStartFrame)
                sx = candidateStartFrame(sx_index);

                V1 = VOLUME_gpu(sub2ind(size(VOLUME_gpu), repmat(x_indices(:,:,1),1,1,1,px), repmat(x_indices(:,:,2),1,1,1,px), ones(FRAME_HEIGHT,FRAME_WIDTH,1,px), TimeMapping(sx,px,repmat(reshape(1:px,[1,1,1,px]),[FRAME_HEIGHT,FRAME_WIDTH,1,1]))));
                V2 = VOLUME_gpu(sub2ind(size(VOLUME_gpu), repmat(x_indices(:,:,1),1,1,1,px), repmat(x_indices(:,:,2),1,1,1,px), ones(FRAME_HEIGHT,FRAME_WIDTH,1,px), TimeMapping(repmat(sz,[1,1,1,px]),px,repmat(reshape(1:px,[1,1,1,px]),[FRAME_HEIGHT,FRAME_WIDTH,1,1]))));

                V3 = VOLUME_gpu(sub2ind(size(VOLUME_gpu), repmat(z_indices(:,:,1),1,1,1,px), repmat(z_indices(:,:,2),1,1,1,px), ones(FRAME_HEIGHT,FRAME_WIDTH,1,px), TimeMapping(sx,px,repmat(reshape(1:px,[1,1,1,px]),[FRAME_HEIGHT,FRAME_WIDTH,1,1]))));
                V4 = VOLUME_gpu(sub2ind(size(VOLUME_gpu), repmat(z_indices(:,:,1),1,1,1,px), repmat(z_indices(:,:,2),1,1,1,px), ones(FRAME_HEIGHT,FRAME_WIDTH,1,px), TimeMapping(repmat(sz,[1,1,1,px]),px,repmat(reshape(1:px,[1,1,1,px]),[FRAME_HEIGHT,FRAME_WIDTH,1,1]))));

                result = reshape(sum((V1 - V2) .^ 2 + (V3 - V4) .^ 2, 4), [FRAME_HEIGHT,FRAME_WIDTH]);
                spatial_cost_gpu(:,:,sx_index) = spatial_cost_gpu(:,:,sx_index) + (result .* gamma_s ./ px .* z_mask);
            end
        end

        spatial_cost_gpu(spatial_cost_gpu <= 1/255) = 0;

        spatial_cost = gather(spatial_cost_gpu);
 
        spatiotemporalCost = SpatialRate * spatial_cost + TemporalRate * temporalCost;
        
    %% multi-label graphcut
        SmoothnessCost = (ones(length(candidateStartFrame)) - eye(length(candidateStartFrame))) * Smoothness;

        [gch] = GraphCut('open', spatiotemporalCost, SmoothnessCost);
        [gch,l] = GraphCut('expand',gch);
        gch = GraphCut('close', gch);
        l = l + 1; % 0-based index => 1-based index
        
        OSF(:,:,px_index) = candidateStartFrame(l);

    %% select px whose loop L_px has lowest spatiotemporal cost at pixel x

        % index vectors for rows and columns
        p = 1:FRAME_HEIGHT;
        q = 1:FRAME_WIDTH;
        % index matrices for rows and columns
        [P, Q] = ndgrid(p, q);

        ind = sub2ind([FRAME_HEIGHT FRAME_WIDTH length(candidateStartFrame)], P, Q, l);
        spatiotemporals(:,:,px_index) = spatiotemporalCost(ind); 
    end
end
toc;
OPTIMAL_START_FRAME = OSF;
smoothedST = imgaussfilt(spatiotemporals, [0.9, 0.9]);

ll = GCMexCut(smoothedST, (ones(length(pxs)) - eye(length(pxs))) * Smoothness);

PERIODS = pxs(ll);

SaveSquence(OPTIMAL_START_FRAME, cat(2, outputfilename, '\OPTIMAL_START_FRAME'), 'OPTIMAL_START_FRAME');

save(cat(2, outputfilename, '\result_stage_one'));

init_P = PERIODS;

disp('Start stage two...');

labels = [sxs pxs];

for iter = 1:2
    
    tic;
    spatial_cost_gpu = ComputeSpatialCost(VOLUME,FRAME_HEIGHT,FRAME_WIDTH,FRAME_COUNT,OPTIMAL_START_FRAME,START_FRAME,PERIODS,sxs,pxs,VC, VC_2);
    spatial_cost_gpu = round(spatial_cost_gpu, 5);
    spatial_cost = gather(spatial_cost_gpu);
    temporal_cost = ComputeTemporalCost(VOLUME,FRAME_HEIGHT,FRAME_WIDTH,FRAME_COUNT,OPTIMAL_START_FRAME,sxs,pxs);
    
    temporal_cost = round(temporal_cost, 5);
    static_cost = ComputeStaticCost(FRAME_HEIGHT,FRAME_WIDTH,FRAME_COUNT,static_energy,sxs,pxs);
    
    static_cost = round(static_cost, 5);
    toc;
    
    SmoothnessCost = (ones(length(labels)) - eye(length(labels))) * Smoothness;
    spatiotemporal_cost = (SpatialRate * spatial_cost + TemporalRate * temporal_cost + StaticRate * static_cost);
    [gch] = GraphCut('open', spatiotemporal_cost, SmoothnessCost);
    [gch,l] = GraphCut('expand',gch);
    gch = GraphCut('close', gch);
    l = l + 1;
    
    % index vectors for rows and columns
    p = 1:FRAME_HEIGHT;
    q = 1:FRAME_WIDTH;
    % index matrices for rows and columns
    [P, Q] = ndgrid(p, q);
    ind = sub2ind([FRAME_HEIGHT FRAME_WIDTH length(labels)], P, Q, l);
    finalSpatialCost = spatial_cost(ind); 

    % update start frame & period
    for x = 1:FRAME_WIDTH
        for y = 1:FRAME_HEIGHT
            if l(y,x) <= length(sxs)
                PERIODS(y,x) = 1;
                START_FRAME(y,x) = labels(l(y,x));
            else
                PERIODS(y,x) = labels(l(y,x));
                START_FRAME(y,x) = OPTIMAL_START_FRAME(y,x,l(y,x) - length(sxs));
            end
        end
    end
end

figure;imagesc(START_FRAME);
colorbar
colormap(color_set)
title('START FRAME')
figure;imagesc(PERIODS);
colorbar
title('PERIODS')
colormap(color_set)

SaveSquence(spatial_cost_gpu, cat(2, outputfilename, '\spatial_cost'), 'spatial_cost');
SaveSquence(temporal_cost, cat(2, outputfilename, '\temporal_cost'), 'temporal_cost');
SaveSquence(temporal_cost+spatial_cost_gpu, cat(2, outputfilename, '\spatialtemporal_cost'), 'spatialtemporal_cost');
% keyboard;

PERIODS(PERIODS > 1) = PERIODS(PERIODS > 1) * 4;
START_FRAME = (START_FRAME - 1) * 4 + 1;

PERIODS = floor(imresize(PERIODS, 1/VIDEO_SCALE,'nearest'));
START_FRAME = floor(imresize(START_FRAME, 1/VIDEO_SCALE,'nearest'));

[FRAME_HEIGHT, FRAME_WIDTH] = size(PERIODS);

save(cat(2, outputfilename, '\result_stage_two'));

CreateVideoLoop(inputfilename,outputfilename, 1)

end