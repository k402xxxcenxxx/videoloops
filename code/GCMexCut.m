function L = GCMexCut(Dc,Sc)
% GraphCut used by gcmex code
% return 1-based index
%

gch = GraphCut('open', Dc, Sc);
[gch, L] = GraphCut('expand',gch);
gch = GraphCut('close', gch);

% convert to 1-based index
L = L + 1;

end

