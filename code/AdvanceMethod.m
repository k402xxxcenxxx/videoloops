function [] = AdvanceMethod(inputfilename, varargin)
%function [] = AdvanceMethod(inputfilename, Smoothness, SpatialRate, TemporalRate, StaticRate, isNormalize, VideoScale, TemporalScale, isSmoothST)
tic;
p = inputParser;

addRequired(p,'inputfilename',@ischar);
addParameter(p,'Smoothness',1e-8,@isnumeric)
addParameter(p,'SpatialRate',10,@isnumeric)
addParameter(p,'TemporalRate',1,@isnumeric)
addParameter(p,'StaticRate',10,@isnumeric)
addParameter(p,'isNormalize',1,@isnumeric)
addParameter(p,'VideoScale',1/4,@isnumeric)
addParameter(p,'TemporalScale',4,@isnumeric)
addParameter(p,'isSmoothST',1,@isnumeric)
addParameter(p,'DebugLog',1,@isnumeric)

parse(p, inputfilename, varargin{:})

inputfilename = p.Results.inputfilename;
Smoothness = p.Results.Smoothness;
SpatialRate = p.Results.SpatialRate;
TemporalRate = p.Results.TemporalRate;
StaticRate = p.Results.StaticRate;
isNormalize = p.Results.isNormalize;
VideoScale = p.Results.VideoScale;
TemporalScale = p.Results.TemporalScale;
isSmoothST = p.Results.isSmoothST;
DebugLog = p.Results.DebugLog;

outputfilename = cat(2, inputfilename, '_sm', num2str(Smoothness), '_sp', num2str(SpatialRate), '_t', num2str(TemporalRate), '_st', num2str(StaticRate), '_nr', num2str(isNormalize), '_vs', num2str(VideoScale), '_ts', num2str(TemporalScale), '_sST', num2str(isSmoothST));

color_set = cat(1,summer,flipud(spring));
color_set(1,:) = [0.5 0.5 0.5];

% Constant variable

global VOLUME
global START_FRAME
global PERIODS
global FRAME_COUNT
global FRAME_HEIGHT
global FRAME_WIDTH

global LAMBDA_S;
LAMBDA_S = 100;
global LAMBDA_T;
LAMBDA_T = 400;
global LAMBDA_static;
LAMBDA_static = 100;
global MIN_PERIODS;
MIN_PERIODS = 32;
global PERIODS_MULTIPLES;
PERIODS_MULTIPLES = 4;
global VIDEO_SCALE
VIDEO_SCALE = VideoScale;
global TEMPORAL_SCALE
TEMPORAL_SCALE = TemporalScale;

Dir_x = [-1,1,0,0];
Dir_y = [0,0,-1,1];

% read video
if isempty(inputfilename)
    read_video();
    outputfilename = cat(2,'bar_move', outputfilename);
else
    read_video(inputfilename);
end

% Color normalize
if isNormalize == 1
    VOLUME = VOLUME ./ 255;
end

% initialize parameter
START_FRAME = ones(FRAME_HEIGHT,FRAME_WIDTH);
PERIODS = ones(FRAME_HEIGHT,FRAME_WIDTH);

% The set of candidate S and P
sxs = 1:1:FRAME_COUNT;
sxs = sxs(2:end);
pxs = 8:1:(FRAME_COUNT-1);
pxs = pxs(pxs + sxs(1) < FRAME_COUNT);

% Pre-compute cumulate-sum table, for spatial cost computing accerlation
disp('Start compute VC VC_2...');
VC = cumsum(VOLUME,4);
VC_2 = cumsum((VOLUME .^2), 4);

% Compute static energy, a static panelty table
disp('Start compute_static_cost...');
N = reshape(VOLUME(:,:,1,:),[FRAME_HEIGHT,FRAME_WIDTH,FRAME_COUNT]);
N = imgaussfilt3(N,[0.9,0.9,1.2]);
static_energy = min(1,LAMBDA_static * mad(abs(N(:,:,1:end-1) - N(:,:,2:end)),1,3));
% Trick : let panelty of static part could be exactly zero, 0.1 just hand-
% craft parameter
static_energy(static_energy <= 0.1) = 0;

% Compute gamma_t table
gamma_t = reshape((1 ./ (1 + LAMBDA_T * mad(abs(VOLUME(:,:,1,1:end-1) - VOLUME(:,:,1,2:end)),1,4))),[FRAME_HEIGHT,FRAME_WIDTH]);

%% Stage one 
% Stage one result, for every candidate P, we choose a S
global OPTIMAL_START_FRAME;
OPTIMAL_START_FRAME = ones(FRAME_HEIGHT, FRAME_WIDTH, length(pxs));

% Store spatiotemporal value to initialize P
spatiotemporals = ones(FRAME_HEIGHT, FRAME_WIDTH,length(pxs));

for px_index = 1:(length(pxs))
    px = pxs(px_index);
    candidateStartFrame = sxs(sxs + px <= FRAME_COUNT);% long periods may not have later start frame (sx + px <= frame_count)
    
    if length(candidateStartFrame) <= 1
        sx = candidateStartFrame(1);
        
        spatial_cost = zeros(FRAME_HEIGHT,FRAME_WIDTH);
        temporal_cost = zeros(FRAME_HEIGHT,FRAME_WIDTH);
        
        % V(x,sx)
        v1_index = sx;
        % V(x,sx+px)
        v2_index = sx+px;
        % V(x,sx-1)
        v3_index = sx - 1;
        % V(x,sx+px-1)
        v4_index = sx + px -1;

        % ||V(x,sx) - V(x,sx+px)||^2 + ||V(x,sx-1) - V(x,sx+px-1)||^2
        temporalCost = reshape((VOLUME(:,:,:,v1_index) - VOLUME(:,:,:,v2_index)).^2 + (VOLUME(:,:,:,v3_index) - VOLUME(:,:,:,v4_index)).^2, [FRAME_HEIGHT,FRAME_WIDTH]);
        temporalCost = temporalCost .* gamma_t;
                
        for x_x = 1:FRAME_WIDTH
            for x_y = 1:FRAME_HEIGHT
                % Loop for all z
                for i = 1:length(Dir_x)
                    % Check boundary
                    if x_x + Dir_x(i) > 0 && x_x + Dir_x(i) <= FRAME_WIDTH
                        if x_y + Dir_y(i) > 0 && x_y + Dir_y(i) <= FRAME_HEIGHT

                            z_x = x_x + Dir_x(i);
                            z_y = x_y + Dir_y(i);

                            pz = px;
                            sz = sx;
                            
                            gamma_s = 1 / (1 + LAMBDA_S * mad(abs(double(VOLUME(x_y,x_x,1,:)) - double(VOLUME(z_y,z_x,1,:))),1,4));

                            result = 0;
                            for t = 1:px
                                result = result + sum((VOLUME(x_y, x_x, 1, TimeMapping(sx,px,t)) - VOLUME(x_y, x_x, 1, TimeMapping(sz,pz,t))).^2 + (VOLUME(z_y, z_x, 1, TimeMapping(sx,px,t)) - VOLUME(z_y, z_x, 1, TimeMapping(sz,pz,t))).^2,4);
                            end

                            spatial_cost(x_y, x_x) = spatial_cost(x_y, x_x) + (result / px * gamma_s);
                            % Loop for label end
                        end
                    end
                end
            end
        end
        
        spatiotemporals(:,:,px_index) = SpatialRate * spatial_cost + TemporalRate * temporalCost;
        OPTIMAL_START_FRAME(:,:,px_index) = sx;
        
    else

        % V(x,sx)
        v1_index = candidateStartFrame;
        % V(x,sx+px)
        v2_index = min(FRAME_COUNT, candidateStartFrame + px);
        % V(x,sx-1)
        v3_index = max(1, candidateStartFrame - 1); % must be positive
        % V(x,sx+px-1)
        v4_index = candidateStartFrame + px -1;

        % ||V(x,sx) - V(x,sx+px)||^2 + ||V(x,sx-1) - V(x,sx+px-1)||^2
        temporalCost = reshape((VOLUME(:,:,:,v1_index) - VOLUME(:,:,:,v2_index)).^2 + (VOLUME(:,:,:,v3_index) - VOLUME(:,:,:,v4_index)).^2, [FRAME_HEIGHT,FRAME_WIDTH,length(candidateStartFrame)]);
        temporalCost = temporalCost .* repmat(gamma_t,1,1,length(candidateStartFrame));

        % Use min temporalCost to initialize S
        [minValues,minIndics] = min(temporalCost,[],3);
        % Table of suboptimal S that we can compute spatial cost
        sub_optimal_start_frame = candidateStartFrame(minIndics);
        
        % Compute spatial cost
        spatial_cost = zeros(FRAME_HEIGHT,FRAME_WIDTH,length(candidateStartFrame));


        x_indices = cat(3,repmat((1:FRAME_HEIGHT)',1,FRAME_WIDTH),repmat(1:FRAME_WIDTH,FRAME_HEIGHT,1));
        for i = 1:length(Dir_x)
            z_indices = cat(3,min(max(x_indices(:,:,1) + Dir_y(i),1),FRAME_HEIGHT),min(max(x_indices(:,:,2) + Dir_x(i),1),FRAME_WIDTH));
            sz = sub_optimal_start_frame(sub2ind([FRAME_HEIGHT,FRAME_WIDTH],z_indices(:,:,1),z_indices(:,:,2)));

            z_mask = zeros(FRAME_HEIGHT,FRAME_WIDTH);
            z_mask(x_indices(:,:,1) + Dir_y(i) <= FRAME_HEIGHT & x_indices(:,:,1) + Dir_y(i) >= 1 & x_indices(:,:,2) + Dir_x(i) <= FRAME_WIDTH & x_indices(:,:,2) + Dir_x(i) >= 1) = 1;

            gamma_s = 1 ./ (1 + LAMBDA_S * mad(abs(VOLUME - VOLUME(sub2ind(size(VOLUME),repmat(z_indices(:,:,1),[1,1,1,FRAME_COUNT]),repmat(z_indices(:,:,2),[1,1,1,FRAME_COUNT]), ones(FRAME_HEIGHT,FRAME_WIDTH,1,FRAME_COUNT), repmat(reshape(1:FRAME_COUNT,[1,1,1,FRAME_COUNT]),[FRAME_HEIGHT,FRAME_WIDTH,1,1])))),1,4));

            for sx_index = 1:length(candidateStartFrame)
                sx = candidateStartFrame(sx_index);

                V1 = VOLUME(sub2ind(size(VOLUME), repmat(x_indices(:,:,1),1,1,1,px), repmat(x_indices(:,:,2),1,1,1,px), ones(FRAME_HEIGHT,FRAME_WIDTH,1,px), TimeMapping(sx,px,repmat(reshape(1:px,[1,1,1,px]),[FRAME_HEIGHT,FRAME_WIDTH,1,1]))));
                V2 = VOLUME(sub2ind(size(VOLUME), repmat(x_indices(:,:,1),1,1,1,px), repmat(x_indices(:,:,2),1,1,1,px), ones(FRAME_HEIGHT,FRAME_WIDTH,1,px), TimeMapping(repmat(sz,[1,1,1,px]),px,repmat(reshape(1:px,[1,1,1,px]),[FRAME_HEIGHT,FRAME_WIDTH,1,1]))));

                V3 = VOLUME(sub2ind(size(VOLUME), repmat(z_indices(:,:,1),1,1,1,px), repmat(z_indices(:,:,2),1,1,1,px), ones(FRAME_HEIGHT,FRAME_WIDTH,1,px), TimeMapping(sx,px,repmat(reshape(1:px,[1,1,1,px]),[FRAME_HEIGHT,FRAME_WIDTH,1,1]))));
                V4 = VOLUME(sub2ind(size(VOLUME), repmat(z_indices(:,:,1),1,1,1,px), repmat(z_indices(:,:,2),1,1,1,px), ones(FRAME_HEIGHT,FRAME_WIDTH,1,px), TimeMapping(repmat(sz,[1,1,1,px]),px,repmat(reshape(1:px,[1,1,1,px]),[FRAME_HEIGHT,FRAME_WIDTH,1,1]))));

                result = reshape(sum((V1 - V2) .^ 2 + (V3 - V4) .^ 2, 4), [FRAME_HEIGHT,FRAME_WIDTH]);
                spatial_cost(:,:,sx_index) = spatial_cost(:,:,sx_index) + (result .* gamma_s ./ px .* z_mask);
            end
        end
        % Trick : let tiny spatial cost could be exactly zero, 1/255 just
        % hand-crafted 
        spatial_cost(spatial_cost <= 1/255) = 0;
 
        spatiotemporal_cost = SpatialRate * spatial_cost + TemporalRate * temporalCost;
        if DebugLog == 1
            if px_index == 1 || px_index == floor(0.5 * (1 + (length(pxs)-1)))
                SaveSquence(spatial_cost, cat(2, outputfilename, '\spatial_cost_stage_one_px', num2str(px)), 'spatial_cost');
                SaveSquence(temporalCost, cat(2, outputfilename, '\temporal_cost_stage_one_px', num2str(px)), 'temporal_cost');
                SaveSquence(spatiotemporal_cost, cat(2, outputfilename, '\spatialtemporal_cost_stage_one_px', num2str(px)), 'spatialtemporal_cost');
            end
        end
        
        % multi-label graphcut
        l = GCMexCut(spatiotemporal_cost, (ones(length(candidateStartFrame)) - eye(length(candidateStartFrame))) * Smoothness);
        
        OPTIMAL_START_FRAME(:,:,px_index) = candidateStartFrame(l);

        % select px whose loop L_px has lowest spatiotemporal cost at pixel x
        p = 1:FRAME_HEIGHT;
        q = 1:FRAME_WIDTH;
        [P, Q] = ndgrid(p, q);
        ind = sub2ind([FRAME_HEIGHT FRAME_WIDTH length(candidateStartFrame)], P, Q, l);
        spatiotemporals(:,:,px_index) = spatiotemporal_cost(ind); 
        
    end
end

% initialize periods by choosing p that has lowest spatiotemporal cost
% Trick : smooth spatiotemporal before graphcut
if isSmoothST == 1
    smoothedST = imgaussfilt(spatiotemporals, [0.9, 0.9]);
    l = GCMexCut(smoothedST, (ones(length(pxs)) - eye(length(pxs))) * Smoothness);
else
    l = GCMexCut(spatiotemporals, (ones(length(pxs)) - eye(length(pxs))) * Smoothness);
end
PERIODS = pxs(l);

if DebugLog == 1
    SaveSquence(OPTIMAL_START_FRAME, cat(2, outputfilename, '\OPTIMAL_START_FRAME'), 'OPTIMAL_START_FRAME');
    save(cat(2, outputfilename, '\result_stage_one'));
end

init_P = PERIODS;

%% Stage two
labels = [sxs pxs];

for iter = 1:2
    
    spatial_cost = zeros(FRAME_HEIGHT,FRAME_WIDTH,length(labels));
    temporal_cost = zeros(FRAME_HEIGHT,FRAME_WIDTH,length(labels));
    static_cost = zeros(FRAME_HEIGHT,FRAME_WIDTH,length(labels));

    % Loop for all pixel
    for x_x = 1:FRAME_WIDTH
        for x_y = 1:FRAME_HEIGHT
            % Compute spatial cost
            for i = 1:length(Dir_x)
                if x_x + Dir_x(i) > 0 && x_x + Dir_x(i) <= FRAME_WIDTH
                    if x_y + Dir_y(i) > 0 && x_y + Dir_y(i) <= FRAME_HEIGHT
                        z_x = x_x + Dir_x(i);
                        z_y = x_y + Dir_y(i);

                        pz = PERIODS(z_y,z_x);
                        if pz <= 1
                            sz = START_FRAME(z_y,z_x); % Use previous result
                        else
                            sz = OPTIMAL_START_FRAME(z_y,z_x, pxs == pz);
                        end

                        gamma_s = 1 / (1 + LAMBDA_S * mad(abs(double(VOLUME(x_y,x_x,1,:)) - double(VOLUME(z_y,z_x,1,:))),1,4));

                        % Loop for all label I need to compute
                        for label_index = 1:length(labels)
                            if label_index <= length(sxs)
                                px = 1;
                                sx = labels(label_index);
                            else
                                px = labels(label_index);
                                sx = double(OPTIMAL_START_FRAME(x_y,x_x, pxs == px));
                            end

                            % Four case
                            if px == 1 && pz == 1
                                result = sum((VOLUME(x_y, x_x, 1, sx) - VOLUME(x_y, x_x, 1, sz)).^2 + ...
                                    (VOLUME(z_y, z_x, 1, sx) - VOLUME(z_y, z_x, 1, sz)).^2, 4);

                                spatial_cost(x_y, x_x, label_index) = spatial_cost(x_y, x_x, label_index) + (result * gamma_s);

                            elseif px == 1 && pz ~= 1
                                % 
                                result = reshape(...
                                VOLUME(x_y, x_x, 1, sx).^2 - 2 * VOLUME(x_y, x_x, 1, sx) / pz * (VC(x_y, x_x, 1, sz+pz-1) - VC(x_y, x_x, 1, sz) + VOLUME(x_y, x_x, 1, sz)) + 1 / pz * (VC_2(x_y, x_x, 1, sz+pz-1) - VC_2(x_y, x_x, 1, sz) + VOLUME(x_y, x_x, 1, sz).^2) + ...
                                VOLUME(z_y, z_x, 1, sx).^2 - 2 * VOLUME(z_y, z_x, 1, sx) / pz * (VC(z_y, z_x, 1, sz+pz-1) - VC(z_y, z_x, 1, sz) + VOLUME(z_y, z_x, 1, sz)) + 1 / pz * (VC_2(z_y, z_x, 1, sz+pz-1) - VC_2(z_y, z_x, 1, sz) + VOLUME(z_y, z_x, 1, sz).^2)...
                                ,[1,1]);

                                spatial_cost(x_y, x_x, label_index) = spatial_cost(x_y, x_x, label_index) + (result * gamma_s);

                            elseif px == pz

                                result = 0;
                                for t = 1:px
                                    result = result + sum((VOLUME(x_y, x_x, 1, TimeMapping(sx,px,t)) - VOLUME(x_y, x_x, 1, TimeMapping(sz,pz,t))).^2 + (VOLUME(z_y, z_x, 1, TimeMapping(sx,px,t)) - VOLUME(z_y, z_x, 1, TimeMapping(sz,pz,t))).^2,4);
                                end

                                spatial_cost(x_y, x_x, label_index) = spatial_cost(x_y, x_x, label_index) + (result / px * gamma_s);

                            else
                                %
                                result = ...
                                1/px * (VC_2(x_y, x_x, 1, sx+px-1) - VC_2(x_y, x_x, 1, sx) + VOLUME(x_y, x_x, 1, sx).^2) + 1/pz * (VC_2(x_y, x_x, 1, sz+pz-1) - VC_2(x_y, x_x, 1, sz) + VOLUME(x_y, x_x, 1, sz).^2) - 2/px/pz * (VC(x_y, x_x, 1, sx+px-1) - VC(x_y, x_x, 1, sx) + VOLUME(x_y, x_x, 1, sx)) * (VC(x_y, x_x, 1, sz+pz-1) - VC(x_y, x_x, 1, sz) + VOLUME(x_y, x_x, 1, sz)) + ...
                                1/px * (VC_2(z_y, z_x, 1, sx+px-1) - VC_2(z_y, z_x, 1, sx) + VOLUME(z_y, z_x, 1, sx).^2) + 1/pz * (VC_2(z_y, z_x, 1, sz+pz-1) - VC_2(z_y, z_x, 1, sz) + VOLUME(z_y, z_x, 1, sz).^2) - 2/px/pz * (VC(z_y, z_x, 1, sx+px-1) - VC(z_y, z_x, 1, sx) + VOLUME(z_y, z_x, 1, sx)) * (VC(z_y, z_x, 1, sz+pz-1) - VC(z_y, z_x, 1, sz) + VOLUME(z_y, z_x, 1, sz));

                                spatial_cost(x_y, x_x, label_index) = spatial_cost(x_y, x_x, label_index) + (result * gamma_s);

                            end

                        end
                        % Loop for label end
                    end
                end
            end

            % Compute temporal cost and static cost
            for label_index = 1:length(labels)
                if label_index <= length(sxs)
                    % If static, temporal_cost = 0, and panelty with static_cost
                    temporal_cost(x_y,x_x,label_index) = 0;
                    static_cost(x_y,x_x,label_index) = static_energy(x_y,x_x);
                else
                    px = labels(label_index);
                    sx = OPTIMAL_START_FRAME(x_y,x_x,(pxs == px));

                    temporal_cost(x_y,x_x,label_index) = gamma_t(x_y,x_x) * sum((VOLUME(x_y,x_x,1,sx) - VOLUME(x_y,x_x,1,min(sx+px,FRAME_COUNT)))^2 + (VOLUME(x_y,x_x,1,max(sx-1,1)) - VOLUME(x_y,x_x,1,sx+px-1))^2,4);

                end

            end
        end
    end
    
    % Trick : descard tiny digit value
    spatial_cost = round(spatial_cost, 5);
    temporal_cost = round(temporal_cost, 5);
    static_cost = round(static_cost, 5);

    % Multi-lebal graphcut
    spatiotemporal_cost = (SpatialRate * spatial_cost + TemporalRate * temporal_cost + StaticRate * static_cost);
    l = GCMexCut(spatiotemporal_cost, (ones(length(labels)) - eye(length(labels))) * Smoothness);
    
    % Update start frame & period
    for x = 1:FRAME_WIDTH
        for y = 1:FRAME_HEIGHT
            if l(y,x) <= length(sxs)
                PERIODS(y,x) = 1;
                START_FRAME(y,x) = labels(l(y,x));
            else
                PERIODS(y,x) = labels(l(y,x));
                START_FRAME(y,x) = OPTIMAL_START_FRAME(y,x,l(y,x) - length(sxs));
            end
        end
    end
end

if DebugLog == 1
    SaveSquence(spatial_cost, cat(2, outputfilename, '\spatial_cost'), 'spatial_cost');
    SaveSquence(temporal_cost, cat(2, outputfilename, '\temporal_cost'), 'temporal_cost');
    SaveSquence(temporal_cost+spatial_cost, cat(2, outputfilename, '\spatialtemporal_cost'), 'spatialtemporal_cost');
end

% Remap to real number
PERIODS(PERIODS > 1) = PERIODS(PERIODS > 1) * TEMPORAL_SCALE;
START_FRAME = (START_FRAME - 1) * TEMPORAL_SCALE + 1;

% Up sampling
PERIODS = floor(imresize(PERIODS, 1/VIDEO_SCALE,'nearest'));
START_FRAME = floor(imresize(START_FRAME, 1/VIDEO_SCALE,'nearest'));

[FRAME_HEIGHT, FRAME_WIDTH] = size(PERIODS);
FRAME_COUNT = FRAME_COUNT*TEMPORAL_SCALE;

% Show the result
figure;imagesc(START_FRAME);
colorbar
colormap(color_set)
title('START FRAME')
figure;imagesc(PERIODS);
colorbar
title('PERIODS')
colormap(color_set)

if DebugLog == 1
    imwrite(ind2rgb(START_FRAME, parula(256)), cat(2, outputfilename, '\START_FRAME.jpg'));
    imwrite(ind2rgb(PERIODS, parula(256)), cat(2, outputfilename, '\PERIODS.jpg'));
    
    save(cat(2, outputfilename, '\result_stage_two'));
end
toc;

% Create video
CreateVideoLoop(inputfilename,outputfilename, 1)

end