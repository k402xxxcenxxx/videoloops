function smoothedA = SuperpixelClustering(A, N, label)
% Use superpixel to clustering data
% A : grayscale data
% N : Desired number of superpixels, specified as a numeric scalar.
% return grayscale smoothed result
    [L,N] = superpixels(A, N);

    smoothedA = zeros(size(A),'like',A);
    idx = label2idx(L);

    for labelVal = 1:N
        grayIdx = idx{labelVal};

        patch = label(grayIdx);
        mask = (patch > 1);

        patch(mask) = mode(patch(mask));

        smoothedA(grayIdx) = patch;
    end
    
    BW = boundarymask(L);
    imshow(imoverlay(A*255,BW,'cyan'),'InitialMagnification',67)
end