function [temporal_cost] = ComputeTemporalCost(V,fh,fw,fc,optimal_start_frame,sxs,pxs)

LAMBDA_T = 400;
% pxs = 32:4:fc;
labels = [sxs pxs];
temporal_cost = zeros(fh,fw,length(labels));


t1 = V(:,:,:,1:end-1);
t2 = V(:,:,:,2:end);
gamma_t = reshape((1 ./ (1 + LAMBDA_T * mad(abs(t1-t2),1,4))),[fh,fw]);

% Loop for all pixel
for x_x = 1:fw
    for x_y = 1:fh
        
        for label_index = 1:length(labels)
            
            if label_index <= length(sxs)
                sx = labels(label_index);
                px = 1;
                
                temporal_cost(x_y,x_x,label_index) = 0;
            else
                px = labels(label_index);
                sx = optimal_start_frame(x_y,x_x,(pxs == px));
                
                temporal_cost(x_y,x_x,label_index) = gamma_t(x_y,x_x) * sum((V(x_y,x_x,1,sx) - V(x_y,x_x,1,min(sx+px,fc)))^2 + (V(x_y,x_x,1,max(sx-1,1)) - V(x_y,x_x,1,sx+px-1))^2,4);
                if sx+px > fc || sx - 1 < 0
                    temporal_cost(x_y,x_x,label_index) = 1e5;
                end
            end
            
        end
    end
end
end

