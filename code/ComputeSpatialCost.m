function [spatial_cost] = ComputeSpatialCost(V,fh,fw,fc,optimal_start_frame,s,p,sxs,pxs,VC,VC_2)

LAMBDA_S = 100;
disp('Start compute_spatial_cost...');

Dir_x = [-1,1,0,0];
Dir_y = [0,0,-1,1];
labels = [sxs pxs];

spatial_cost = zeros(fh,fw,length(labels));

% Loop for all pixel
for x_x = 1:fw
    for x_y = 1:fh

        % Loop for all z
        for i = 1:length(Dir_x)
            if x_x + Dir_x(i) > 0 && x_x + Dir_x(i) <= fw
                if x_y + Dir_y(i) > 0 && x_y + Dir_y(i) <= fh
                    z_x = x_x + Dir_x(i);
                    z_y = x_y + Dir_y(i);

                    pz = p(z_y,z_x);
                    sz = 1;
                    if pz <= 1
                        sz = s(z_y,z_x); % Use previous result
                    else
                        sz = double(optimal_start_frame(z_y,z_x, pxs == pz));
                    end

                    gamma_s = 1 / (1 + LAMBDA_S * mad(abs(double(V(x_y,x_x,1,:)) - double(V(z_y,z_x,1,:))),1,4));
                    
                    % Loop for all label I need to compute
                    for label_index = 1:length(labels)
                        px = 1;
                        sx = 1;
                        if label_index <= length(sxs)
                            px = 1;
                            sx = labels(label_index);
                        else
                            px = labels(label_index);
                            sx = double(optimal_start_frame(x_y,x_x, pxs == px));
                        end
                        
%                         T = lcm(px,pz);
%                         result = 0;
%                         for t = 1:T
%                             result = result + (V(x_y,x_x,1,TimeMapping(sx,px,t)) - V(x_y,x_x,1,TimeMapping(sz,pz,t))).^2 + ...
%                                               (V(z_y,z_x,1,TimeMapping(sx,px,t)) - V(z_y,z_x,1,TimeMapping(sz,pz,t))).^2;
%                         end
%                         spatial_cost(x_y,x_x,label_index) = spatial_cost(x_y,x_x,label_index) + result * gamma_s / T;
% 
                        % Four case
                        if px == 1 && pz == 1
                            result = sum((V(x_y, x_x, 1, sx) - V(x_y, x_x, 1, sz)).^2 + ...
                                (V(z_y, z_x, 1, sx) - V(z_y, z_x, 1, sz)).^2, 4);
                            
                            spatial_cost(x_y, x_x, label_index) = spatial_cost(x_y, x_x, label_index) + (result * gamma_s);
                                
                        elseif px == 1 && pz ~= 1
                            % 
                            result = ...
                            V(x_y, x_x, 1, sx).^2 - 2 * V(x_y, x_x, 1, sx) / pz * (VC(x_y, x_x, 1, sz+pz-1) - VC(x_y, x_x, 1, sz) + V(x_y, x_x, 1, sz)) + 1 / pz * (VC_2(x_y, x_x, 1, sz+pz-1) - VC_2(x_y, x_x, 1, sz) + V(x_y, x_x, 1, sz).^2) + ...
                            V(z_y, z_x, 1, sx).^2 - 2 * V(z_y, z_x, 1, sx) / pz * (VC(z_y, z_x, 1, sz+pz-1) - VC(z_y, z_x, 1, sz) + V(z_y, z_x, 1, sz)) + 1 / pz * (VC_2(z_y, z_x, 1, sz+pz-1) - VC_2(z_y, z_x, 1, sz) + V(z_y, z_x, 1, sz).^2);
                            
                            spatial_cost(x_y, x_x, label_index) = spatial_cost(x_y, x_x, label_index) + (result * gamma_s);

                        elseif px == pz

                            result = 0;
                            for t = 1:px
                                result = result + sum((V(x_y, x_x, 1, TimeMapping(sx,px,t)) - V(x_y, x_x, 1, TimeMapping(sz,pz,t))).^2 + (V(z_y, z_x, 1, TimeMapping(sx,px,t)) - V(z_y, z_x, 1, TimeMapping(sz,pz,t))).^2,4);
                            end

                            spatial_cost(x_y, x_x, label_index) = spatial_cost(x_y, x_x, label_index) + (result / px * gamma_s);

                        else
                            %
                            result = ...
                            1/px * (VC_2(x_y, x_x, 1, sx+px-1) - VC_2(x_y, x_x, 1, sx) + V(x_y, x_x, 1, sx).^2) + 1/pz * (VC_2(x_y, x_x, 1, sz+pz-1) - VC_2(x_y, x_x, 1, sz) + V(x_y, x_x, 1, sz).^2) - 2/px/pz * (VC(x_y, x_x, 1, sx+px-1) - VC(x_y, x_x, 1, sx) + V(x_y, x_x, 1, sx)) * (VC(x_y, x_x, 1, sz+pz-1) - VC(x_y, x_x, 1, sz) + V(x_y, x_x, 1, sz)) + ...
                            1/px * (VC_2(z_y, z_x, 1, sx+px-1) - VC_2(z_y, z_x, 1, sx) + V(z_y, z_x, 1, sx).^2) + 1/pz * (VC_2(z_y, z_x, 1, sz+pz-1) - VC_2(z_y, z_x, 1, sz) + V(z_y, z_x, 1, sz).^2) - 2/px/pz * (VC(z_y, z_x, 1, sx+px-1) - VC(z_y, z_x, 1, sx) + V(z_y, z_x, 1, sx)) * (VC(z_y, z_x, 1, sz+pz-1) - VC(z_y, z_x, 1, sz) + V(z_y, z_x, 1, sz));

                            spatial_cost(x_y, x_x, label_index) = spatial_cost(x_y, x_x, label_index) + (result * gamma_s);

                        end

                    end
                    % Loop for label end
                end
            end
        end
        % Loop for all z end
    end
end
% Loop for all pixel end

disp('Finish compute_spatial_cost...');

end

