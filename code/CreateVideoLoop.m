function [] = CreateVideoLoop(input_file, output_file, isGray)
global VOLUME
global START_FRAME
global PERIODS
global FRAME_COUNT
global FRAME_HEIGHT
global FRAME_WIDTH
global FRAME_CHANNEL
global FRAME_SIZE
global VIDEO_SCALE
global TEMPORAL_SCALE

if isempty(input_file)
    VIDEO_SCALE = 1;
    TEMPORAL_SCALE = 1;
    read_video();
else
xyloObj = VideoReader(input_file);

VIDEO_SCALE = 1;

FRAME_WIDTH = xyloObj.Width *VIDEO_SCALE;
FRAME_HEIGHT = xyloObj.Height*VIDEO_SCALE;
% FRAME_CHANNEL = 1;

k = 1;
if isGray == 1
    FRAME_CHANNEL = 1;
    VOLUME = zeros(FRAME_HEIGHT,FRAME_WIDTH,1,FRAME_COUNT,'uint8');
    while hasFrame(xyloObj)
        VOLUME(:,:,:,k) = imresize(rgb2gray(readFrame(xyloObj)),VIDEO_SCALE);
        k = k+1;
    end
else
    FRAME_CHANNEL = 3;
    VOLUME = zeros(FRAME_HEIGHT,FRAME_WIDTH,3,FRAME_COUNT,'uint8');
    while hasFrame(xyloObj)
        VOLUME(:,:,:,k) = imresize(readFrame(xyloObj),VIDEO_SCALE);
        k = k+1;
    end
end
FRAME_COUNT = k-1;
end

result = load(cat(2,'.\', output_file, '\result_stage_two.mat'));

s = reshape(result.START_FRAME,[FRAME_HEIGHT,FRAME_WIDTH]);
p = reshape(result.PERIODS,[FRAME_HEIGHT,FRAME_WIDTH]);

if isGray == 1

    loop_frames = FRAME_COUNT * 10;
    loop = zeros(FRAME_HEIGHT,FRAME_WIDTH,FRAME_CHANNEL,loop_frames,'uint8');
    % frame = zeros(FRAME_HEIGHT,FRAME_WIDTH,FRAME_CHANNEL,'uint8');

    t = 1:loop_frames;
    x = 1:FRAME_WIDTH;
    y = 1:FRAME_HEIGHT;

    S = reshape(repmat(s,[1,1,length(t)]), [FRAME_HEIGHT*FRAME_WIDTH*length(t), 1]);
    P = reshape(repmat(p,[1,1,length(t)]), [FRAME_HEIGHT*FRAME_WIDTH*length(t), 1]);
    T = reshape(repmat(reshape(t,[1,1,length(t)]),[FRAME_HEIGHT,FRAME_WIDTH,1]), [FRAME_HEIGHT*FRAME_WIDTH*length(t), 1]);
    T = reshape(TimeMapping_video(S,P,T),[FRAME_HEIGHT,FRAME_WIDTH,1,length(t)]);

    ind = sub2ind(size(VOLUME), repmat(y',[1, FRAME_WIDTH,1,length(t)]), repmat(x,[FRAME_HEIGHT,1,1,length(t)]),ones(FRAME_HEIGHT,FRAME_WIDTH,1,length(t)), T);
    loop = reshape(double(VOLUME(ind)) ./ 255,[FRAME_HEIGHT,FRAME_WIDTH,1,length(t)]);
    
    
    vw = VideoWriter(cat(2, output_file,'\loop_gray.avi'));
    open(vw);
    writeVideo(vw,loop);
    close(vw);
else
    
    loop_frames = FRAME_COUNT * 10;
    loop = zeros(FRAME_HEIGHT,FRAME_WIDTH,FRAME_CHANNEL,loop_frames,'uint8');
    
    t = 1:loop_frames;
    x = 1:FRAME_WIDTH;
    y = 1:FRAME_HEIGHT;

    S = reshape(repmat(s,[1,1,length(t)]), [FRAME_HEIGHT*FRAME_WIDTH*length(t), 1]);
    P = reshape(repmat(p,[1,1,length(t)]), [FRAME_HEIGHT*FRAME_WIDTH*length(t), 1]);
    T = reshape(repmat(reshape(t,[1,1,length(t)]),[FRAME_HEIGHT,FRAME_WIDTH,1]), [FRAME_HEIGHT*FRAME_WIDTH*length(t), 1]);
    T = reshape(TimeMapping_video(S,P,T),[FRAME_HEIGHT,FRAME_WIDTH,length(t)]);
    
    ind = sub2ind([FRAME_HEIGHT FRAME_WIDTH FRAME_CHANNEL FRAME_COUNT], repmat(y',[1, FRAME_WIDTH,length(t)]), repmat(x,[FRAME_HEIGHT,1,length(t)]),ones(FRAME_HEIGHT,FRAME_WIDTH,length(t)), T);
    loop(:,:,1,:) = reshape(double(VOLUME(ind)),[FRAME_HEIGHT,FRAME_WIDTH,1,length(t)]);
    ind = sub2ind([FRAME_HEIGHT FRAME_WIDTH FRAME_CHANNEL FRAME_COUNT], repmat(y',[1, FRAME_WIDTH,length(t)]), repmat(x,[FRAME_HEIGHT,1,length(t)]),ones(FRAME_HEIGHT,FRAME_WIDTH,length(t)) * 2, T);
    loop(:,:,2,:) = reshape(double(VOLUME(ind)),[FRAME_HEIGHT,FRAME_WIDTH,1,length(t)]);
    ind = sub2ind([FRAME_HEIGHT FRAME_WIDTH FRAME_CHANNEL FRAME_COUNT], repmat(y',[1, FRAME_WIDTH,length(t)]), repmat(x,[FRAME_HEIGHT,1,length(t)]),ones(FRAME_HEIGHT,FRAME_WIDTH,length(t)) * 3, T);
    loop(:,:,3,:) = reshape(double(VOLUME(ind)),[FRAME_HEIGHT,FRAME_WIDTH,1,length(t)]);
    
    vw = VideoWriter(cat(2, output_file,'\loop_color.avi'));
    open(vw);
    writeVideo(vw,loop);
    close(vw);
end

end

function result = TimeMapping_video(sx,px,t)

mask = mod(t,px) < mod(sx,px);

result = sx - mod(sx,px) + mod(t,px);
result(mask) = result(mask)+px(mask);
end