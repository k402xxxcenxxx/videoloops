% function ComputeLoop(Smoothness, SpatialRate, TemporalRate, StaticRate)
Smoothness = 0.0; 
SpatialRate = 10;
TemporalRate = 1;
StaticRate = 10;
file_name = 'ball_small_normalize_with_spatial_sm0.0';

color_set = cat(1,summer,flipud(spring));
color_set(1,:) = [0.5 0.5 0.5];

pause = 1;

% Compute spatial cost
global VOLUME
global START_FRAME
global PERIODS
global FRAME_COUNT
global FRAME_HEIGHT
global FRAME_WIDTH

% global TEMPORAL_COST

global VC;
global VC_2;


% 常數
global LAMBDA_S;
LAMBDA_S = 100;
global LAMBDA_T;
LAMBDA_T = 400;
global LAMBDA_static;
LAMBDA_static = 100;
global MIN_PERIODS;
MIN_PERIODS = 32;
global PERIODS_MULTIPLES;
PERIODS_MULTIPLES = 4;

% input video , initialize map
read_video('ball_small.mp4');

% % Color normalize
VOLUME = VOLUME ./ 255;

% % % Crop for water region
% VOLUME = VOLUME(513:end,720:720+32-1,:,:);
% FRAME_HEIGHT = 32;
% FRAME_WIDTH = 32;
% START_FRAME = ones(FRAME_HEIGHT,FRAME_WIDTH);
% PERIODS = ones(FRAME_HEIGHT,FRAME_WIDTH);

% keyboard;
% targetP = imread('targetP.png');
% targetP = targetP(513:end,720:720+32-1,:);
% figure;imagesc(targetP);
% title('Target P');
% 
% targetS = imread('targetS.png');
% targetS = targetS(513:end,720:720+32-1,:);
% figure;imagesc(targetS);
% title('Target S');

disp('Start compute VC VC_2...');
VC = cumsum(VOLUME,4);
VC_2 = cumsum((VOLUME .^2), 4);

disp('Start compute_static_cost...');
tic;

N = reshape(VOLUME(:,:,1,:),[FRAME_HEIGHT,FRAME_WIDTH,FRAME_COUNT]);
N = imgaussfilt3(N,[0.9,0.9,1.2]);

static_energy = min(1,LAMBDA_static * mad(abs(N(:,:,1:end-1) - N(:,:,2:end)),1,3));
figure;imagesc(static_energy);
colorbar
title('static energy')
colormap(color_set)
toc;

sxs = 1:4:FRAME_COUNT;
pxs = 32:4:FRAME_COUNT;

global OPTIMAL_START_FRAME;
OPTIMAL_START_FRAME = ones(FRAME_HEIGHT, FRAME_WIDTH, length(pxs));

disp('Start compute_temporal_cost...');
tic;

t1 = VOLUME(:,:,:,1:end-1);
t2 = VOLUME(:,:,:,2:end);
gamma_t = reshape((1 ./ (1 + LAMBDA_T * mad(abs(t1-t2),1,4))),[FRAME_HEIGHT,FRAME_WIDTH]);

figure;imagesc(gamma_t);
colorbar
title('gamma t')
colormap(color_set)

if exist(cat(2,'.\SaveMat\',file_name,'_stage_one.mat'), 'file') ~= 2
    
% spatial_cost = zeros(FRAME_HEIGHT,FRAME_WIDTH);
% Dir_x = [-1,1,0,0];
% Dir_y = [0,0,-1,1];
% for x_x = 1:FRAME_WIDTH
%     for x_y = 1:FRAME_HEIGHT
% 
%         % Loop for all z
%         for i = 1:length(Dir_x)
%             if x_x + Dir_x(i) > 0 && x_x + Dir_x(i) <= FRAME_WIDTH
%                 if x_y + Dir_y(i) > 0 && x_y + Dir_y(i) <= FRAME_HEIGHT
%                     z_x = x_x + Dir_x(i);
%                     z_y = x_y + Dir_y(i);
% 
%                     pz = px;
%                     sz = sub_optimal_start_frame(z_y,z_x); % Use previous result
% 
%                     gamma_s = 1 / (1 + LAMBDA_S * mad(abs(double(VOLUME(x_y,x_x,1,:)) - double(VOLUME(z_y,z_x,1,:))),1,4));
%                     result = (VOLUME(x_y,x_x,1,sx) - VOLUME(x_y,x_x,1,sz)).^2 + ...
%                              (VOLUME(z_y,z_x,1,sx) - VOLUME(z_y,z_x,1,sz)).^2;
%                     spatial_cost(x_y,x_x) = spatial_cost(x_y,x_x) + result * gamma_s;
%                     % Loop for label end
%                 end
%             end
%         end
%         % Loop for all z end
%     end
% end
% minSpatialTemporalCost = ones(FRAME_HEIGHT,FRAME_WIDTH) * realmax;
minSpatialTemporalCost = static_energy;

% keyboard;

% minTemporalCost = ones(FRAME_HEIGHT,FRAME_WIDTH) * realmax;
for px_index = 1:(length(pxs)-1)
% for px = (2:(FRAME_COUNT-2)) % px 
    px = pxs(px_index);
    candidateStartFrame = sxs(sxs + px <= FRAME_COUNT);% long periods may not have later start frame (sx + px <= frame_count)
%     keyboard;
    % V(x,sx)
    v1_index = candidateStartFrame;
    % V(x,sx+px)
    v2_index = min(FRAME_COUNT, candidateStartFrame + px);
    % V(x,sx-1)
    v3_index = max(1, candidateStartFrame - 1); % must be positive
    % V(x,sx+px-1)
    v4_index = candidateStartFrame + px -1;
    
    % ||V(x,sx) - V(x,sx+px)||^2 + ||V(x,sx-1) - V(x,sx+px-1)||^2
    temporalCost = reshape((VOLUME(:,:,:,v1_index) - VOLUME(:,:,:,v2_index)).^2 + (VOLUME(:,:,:,v3_index) - VOLUME(:,:,:,v4_index)).^2, [FRAME_HEIGHT,FRAME_WIDTH,length(candidateStartFrame)]);
   
    temporalCost = temporalCost .* repmat(gamma_t,1,1,length(candidateStartFrame));
    
    [minValues,minIndics] = min(temporalCost,[],3);
    sub_optimal_start_frame = candidateStartFrame(minIndics);
%     
%     SmoothnessCost = (ones(length(candidateStartFrame)) - eye(length(candidateStartFrame))) * 0.001;
%     [gch] = GraphCut('open', temporalCost, SmoothnessCost);
%     [gch l] = GraphCut('expand',gch);
%     gch = GraphCut('close', gch);
%     l = l + 1;
%     minIndics = l;
%     
%     sub_optimal_start_frame = candidateStartFrame(minIndics);
%     figure;imagesc(sub_optimal_start_frame);
%     keyboard;

%     spatial_cost = zeros(FRAME_HEIGHT,FRAME_WIDTH,length(candidateStartFrame));
    
%     Dir_x = [-1,1,0,0];
%     Dir_y = [0,0,-1,1];
%     for x_x = 1:FRAME_WIDTH
%         for x_y = 1:FRAME_HEIGHT
%             
%             % Loop for all z
%             for i = 1:length(Dir_x)
%                 if x_x + Dir_x(i) > 0 && x_x + Dir_x(i) <= FRAME_WIDTH
%                     if x_y + Dir_y(i) > 0 && x_y + Dir_y(i) <= FRAME_HEIGHT
%                         z_x = x_x + Dir_x(i);
%                         z_y = x_y + Dir_y(i);
% 
%                         pz = px;
%                         sz = sub_optimal_start_frame(z_y,z_x); % Use previous result
% 
%                         gamma_s = 1 / (1 + LAMBDA_S * mad(abs(double(VOLUME(x_y,x_x,1,:)) - double(VOLUME(z_y,z_x,1,:))),1,4));
%                         
%                         for sx_index = 1:length(candidateStartFrame)
%                             sx = candidateStartFrame(sx_index);
%                             
%                             result = 0;
%                             for t = 1:px
%                                 result = result + (VOLUME(x_y,x_x,1,TimeMapping(sx,px,t)) - VOLUME(x_y,x_x,1,TimeMapping(sz,pz,t))).^2 + ...
%                                                   (VOLUME(z_y,z_x,1,TimeMapping(sx,px,t)) - VOLUME(z_y,z_x,1,TimeMapping(sz,pz,t))).^2;
%                             end
%                             spatial_cost(x_y,x_x,sx_index) = spatial_cost(x_y,x_x,sx_index) + result * gamma_s / px;
%                         end
%                         % Loop for label end
%                     end
%                 end
%             end
%             % Loop for all z end
%         end
%     end
    
%     spatiotemporalCost = 10 * spatial_cost + temporalCost;
%     [minValues,minIndics] = min(spatiotemporalCost,[],3);
%     
%     mask = minSpatialTemporalCost > minValues;
%     PERIODS(mask) = px;
%     minSpatialTemporalCost(mask) = minValues(mask);


    spatiotemporalCost = temporalCost;
    [minValues,minIndics] = min(spatiotemporalCost,[],3);
    
    mask = (minSpatialTemporalCost > minValues);
    PERIODS(mask) = px;
    minSpatialTemporalCost(mask) = minValues(mask);

% keyboard;
    SmoothnessCost = (ones(length(candidateStartFrame)) - eye(length(candidateStartFrame))) * Smoothness;
    [gch] = GraphCut('open', spatiotemporalCost, SmoothnessCost);
    [gch l] = GraphCut('expand',gch);
    gch = GraphCut('close', gch);
    l = l + 1;

    % 紀錄每個period時每個pixel的start frame
    OPTIMAL_START_FRAME(:,:,px_index) = l;
    
%     keyboard;
end
    save(cat(2,'.\SaveMat\',file_name,'_stage_one'),'PERIODS','OPTIMAL_START_FRAME','static_energy');
else
    load(cat(2,'.\SaveMat\',file_name,'_stage_one'),'PERIODS','OPTIMAL_START_FRAME','static_energy');
end
% PERIODS(PERIODS<=32) = 1;
imwrite( ind2rgb(im2uint8(mat2gray(PERIODS)), parula(256)),cat(2,'ResultFigure/P/',file_name,'_init.jpg'));
figure;imagesc(PERIODS);
title('init_PERIODS');
% keyboard;
toc;

disp('Start stage two...');

labels = [sxs pxs];

for iter = 1:5
    
    tic;
%     static_cost = ComputeSpatialCost_mex('ComputeStaticCost',FRAME_HEIGHT,FRAME_WIDTH,FRAME_COUNT,static_energy,pxs);

    spatial_cost = ComputeSpatialCost(VOLUME,FRAME_HEIGHT,FRAME_WIDTH,FRAME_COUNT,OPTIMAL_START_FRAME,START_FRAME,PERIODS,sxs,pxs,VC, VC_2);
    temporal_cost = ComputeTemporalCost(VOLUME,FRAME_HEIGHT,FRAME_WIDTH,FRAME_COUNT,OPTIMAL_START_FRAME,sxs,pxs);
    static_cost = ComputeStaticCost(FRAME_HEIGHT,FRAME_WIDTH,FRAME_COUNT,static_energy,sxs,pxs);
    toc;
    
    SmoothnessCost = (ones(length(labels)) - eye(length(labels))) * Smoothness;
    
    [gch] = GraphCut('open', (SpatialRate * spatial_cost + TemporalRate * temporal_cost + StaticRate * static_cost), SmoothnessCost);
    [gch l] = GraphCut('expand',gch);
    gch = GraphCut('close', gch);
    l = l + 1;

    % update start frame & period
    for x = 1:FRAME_WIDTH
        for y = 1:FRAME_HEIGHT
            if l(y,x) <= length(sxs)
                PERIODS(y,x) = 1;
                START_FRAME(y,x) = labels(l(y,x));
            else
                PERIODS(y,x) = labels(l(y,x));
                START_FRAME(y,x) = OPTIMAL_START_FRAME(y,x,l(y,x) - length(sxs));
            end
        end
    end
end

save(cat(2,'.\SaveMat\',file_name,'_stage_two'),'PERIODS','START_FRAME');

% FileName = cat(2,'Sm-',num2str(Smoothness),'-Sp-',num2str(SpatialRate),'-T-',num2str(TemporalRate),'-St-',num2str(StaticRate));

figure;imagesc(START_FRAME);
colorbar
colormap(color_set)
title('START FRAME')
figure;imagesc(PERIODS);
colorbar
title('PERIODS')
colormap(color_set)
