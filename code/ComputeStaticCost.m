function [static_cost] = ComputeStaticCost(fh,fw,fc,static_energy,sxs,pxs)
% pxs = 32:4:fc;
labels = [sxs pxs];
static_cost = zeros(fh,fw,length(labels));

% Loop for all pixel
for x_x = 1:fw
    for x_y = 1:fh
        for start_frame = 1:length(sxs)
            static_cost(x_y,x_x,start_frame) = static_energy(x_y,x_x);
        end
        
    end
end

end

