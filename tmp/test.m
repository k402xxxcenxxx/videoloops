
% Compute spatial cost
global VOLUME
global START_FRAME
global PERIODS
global FRAME_COUNT
global FRAME_HEIGHT
global FRAME_WIDTH

global OPTIMAL_START_FRAME;

 SmoothnessCost = (ones(length(labels)) - eye(length(labels))) * 0;
     
[gch] = GraphCut('open', (SpatialRate * spatial_cost + TemporalRate * temporal_cost + 10 * static_cost), SmoothnessCost);
[gch l] = GraphCut('expand',gch);
gch = GraphCut('close', gch);
l = l + 1;

% update start frame & period
for x = 1:FRAME_WIDTH
    for y = 1:FRAME_HEIGHT
        if l(y,x) <= FRAME_COUNT
            PERIODS(y,x) = 1;
            START_FRAME(y,x) = labels(l(y,x));
        else
            PERIODS(y,x) = labels(l(y,x));
            START_FRAME(y,x) = OPTIMAL_START_FRAME(y,x,l(y,x) - FRAME_COUNT);
        end
    end
end