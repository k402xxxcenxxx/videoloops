# Video Loop GPU Accerleration

## Experiment
+ Environment: windows10 + matlab2018a
+ exec command: AdvanceMethod(<filename>, <smoothness>, <spatialRate>, <temporalRate>, <staticRate>, <isNormalize>)
+ Remember to add path of two folder: GEMex, code
## Parameter Type 
+ filename: string
+ smoothness, spatialRate, temporateRate, staticRate: double
+ (paper recommand spatialRate, temporalRate, staticRate: 10,1,10)
+ isNormalize: 0 or 1

## Team Member
+ 蔡侑儒 r06922009
+ 胡柯民 r06922046
+ 王昱翔 r06944001

## Result
+ undinefalls
https://youtu.be/Sbs8hO9SmV8

+ firehole3
https://youtu.be/WMAvrf2ds-o

+ pool
https://youtu.be/Fn4GfjkT5uo

## Reference
http://hhoppe.com/proj/videoloops/